\documentclass[xetex,t,fleqn,dvipsnames]{beamer}
\usepackage{soul}
\newif\ifgrid
\gridfalse
\input{preamble.tex}
\newcommand{\ideg}{\operatorname{ideg}}
\renewcommand{\epsilon}{\varepsilon}

\usepackage{pdfcomment}
%\newcommand{\pdfnote}[1]{\marginnote{\pdfcomment[icon=note]{#1}}}
\newcommand{\pdfnote}[1]{} 

\begin{document}

\begin{frame}
  \vspace{1em}
  
  \begin{center}
    
    {\fontsize{25}{30}\frametitlefont \textcolor{Primary1}{Derandomization from}}
    \vspace{0.2cm}

    {\fontsize{25}{30}\frametitlefont \textcolor{Primary1}{algebraic hardness}}

    \vspace{0.3cm}

    {\fontsize{15}{12} \fontinsc \textcolor{Primary2}{Treading the borders}}

    \vspace{1.5cm}

    \begin{tabular}{ccc}
      {\bf Zeyu Guo} & & {\bf Mrinal Kumar} \\
      IIT Kanpur $\rightarrow$ U. Haifa & & U. Toronto $\rightarrow$ IITB \\
                         & & \\ 
      {\bf \Emph{Ramprasad Saptharishi}} & & {\bf Noam Solomon}\\
      {\Emph{TIFR, Mumbai}} & & {Harvard University}
    \end{tabular}

    \vspace{1cm}
    IIT Bombay\\

    June 2019
  \end{center}
\pdfnote{My presentation notes. }
\end{frame}

\section{Introduction}

\begin{frame}
  \frametitle{Algebraic Circuits}

  \vskip 10pt
  \tikzstyle{every node} = [minimum size=30pt]
  \tikzstyle{gate}=[circle,draw=black!50,thick]
  \tikzstyle{leaf}=[circle,draw=black!50,fill=black!10,thick]
  \begin{tikzpicture}[scale=0.5, transform shape]
    \node[leaf] (x0) at (-4,0) {{\LARGE $x_1$}};
    \node[leaf] (x1) at (0,0) {{\LARGE $x_2$}};
    \node[leaf] (x2) at (4,0) {{\LARGE $x_3$}};
    \node[gate] (p1) at (-5,6) {{\Large $+$}}
    edge[<-] (x0)
    edge[<-] (x1);
    \node[gate] (p2) at (-3,6) {{\Large $+$}}
    edge[<-] (x0)
    edge[<-] (x2);
    \node[gate] (p3) at (-1,6) {{\Large $+$}}
    edge[<-] (x0);
    \node[gate] (p4) at (1,6) {{\Large $+$}}
    edge[<-] (x0)
    edge[<-] (x1)
    edge[<-] (x2);
    \node[gate] (p5) at (3,6) {{\Large $+$}}
    edge[<-] (x1);
    \node[gate] (p6) at (5,6) {{\Large $+$}}
    edge[<-] (x2);
    \node[gate] (m1) at (-4,10) {{\Large $\times$}}
    edge[<-] (p1)  
    edge[<-] (p2);  
    \node[gate] (m2) at (0,10) {{\Large $\times$}}
    edge[<-] (p3)  
    edge[<-] (p4);  
    \node[gate] (m3) at (4,10) {{\Large $\times$}}
    edge[<-] (p5)  
    edge[<-] (p6);
    \node[gate] (root) at (0,12) {{\Large $+$}}
    edge[<-] (m1)
    edge[<-, bend right] (m2)
    edge[<-, bend left] (m2)
    edge[<-] (m3);
    \draw (root) -- (0,14);
    \node (poly) at (7,14) {\Huge $f(x_1,x_2,x_3)$};
    \draw[->] (0,14) -- (poly);
    \node[text=black!10,anchor=east] at (17,-1) {Aren't we all tired of this picture?};
  \end{tikzpicture}
  \pdfnote{Characteristic zero}
\end{frame}


\begin{frame}
  \frametitle{Two Important Questions}

  \pause
  \begin{itemize}

    \vskip 3em
    
  \item \Emph{\bf Lower Bounds:} Can we find an explicit family of polynomials $\set{P_n}$ that require large circuits?\pause
    
    \vskip 3em

  \item \Emph{\bf Polynomial Identity Testing:} Given a circuit $C$, can we  check if $C$ is computing the \emph{zero} polynomial \Emph{(deterministically)}? 

    \vskip 1em

    \pause
    
    \begin{itemize}
      \item {\bf \Emph{Hitting sets}:} Find a set of points $H$ such that any ``small'' circuit $C$ that is computing a nonzero polynomial \emph{must} satisfy $C(\veca) \neq 0$ for some $\veca \in H$. 
    \end{itemize}
  \end{itemize}

  \vskip 1em

  \pause 
  These two problems are intimately connected to each other. 
  
\end{frame}

\begin{frame}
  \frametitle{A ``trivial'' hitting set}

  \vskip 1em
  
  \begin{lemma}[\Cite{Ore, Demillo-Lipton, Schwartz, Zippel}]
    If $P(x_1,\ldots, x_n)$ is a nonzero polynomial of degree $d$, and $S \subseteq \F$ of size at least $d+1$, then $P(\veca) \neq 0$ for some $\veca \in S^n$.
  \end{lemma}

  \pause\vskip 1.5em

  We have an explicit hitting set of size $(d+1)^n$ for $\mathcal{C}(n,d,\ast)$. 

  \pause\vskip 2.5em

  {\bf Q:} Are there smaller hitting sets for $\mathcal{C}(n,d,s)$?

  \pause{\bf A:} Yes; almost any set of size $O(s^2)$ will work.

  \vskip 1.5em
  
  \pause{\bf Q:} Can you give just one explicit example?

  \pause
  {\bf A:} Umm... 

  \pdfnote{C(n,d,s), mention what that means}
\end{frame}

\begin{frame}
  \frametitle{Pseudorandom objects}

  \vskip 1em
  
  \begin{quote}
    ``How difficult could it be to find hay in a haystack?''

    \hfill  --- Howard Karloff
  \end{quote}


  \vskip 2em

  \only<2->{
    \begin{itemize}
    \item You care a lot about \Emph{\only<2-4,7->{hay}\only<5>{hard polynomials}\only<6>{hitting sets}}.

      \only<3->{
        
        \vskip 1em
        
      \item Almost every\only<3-4,7->{thing in a \Emph{haystack}}\only<5>{ \Emph{polynomial}}\only<6>{ \Emph{set of poly-size}} is \only<5-6>{a }\Emph{\only<3-4,7->{hay}\only<5>{hard polynomial}\only<6>{hitting set}}.
      }
      \only<4->{
        
        \vskip 1em
        
      \item Find \only<5-6>{a }\Emph{\only<4,7->{hay}\only<5>{hard polynomial}\only<6>{hitting set}}.

        \textcolor{black!5}{(Why do we still keep finding needles all the time?)}
      }
    \end{itemize}

        \only<7->{

    \vskip 2em
      {\bf \Emph{Question:}} Can we use one pseudorandom object to build another?
    }
  }
  \pdfnote{ECC, expanders, samplers, extractors}
\end{frame}

\begin{frame}
  \frametitle{Lower bounds \only<1-3>{and}\only<4->{$\rightarrow$} hitting sets}
  \begin{tikzpicture}
    \helpgrid{11}{-5}

    \node[rectangle, thick,draw] (HS) at (5.5,-1) {Hard polynomials};
    \node[rectangle, thick,draw] (LB) at (5.5,-4) {Explicit Hitting Sets};
    \onslide<2->{
      \draw[->, bend left, ultra thick, onslide=<4->{Emph1} ] (HS) to (LB);
      \onslide<2,3>{\node[anchor=west] at (6,-2.5) {\Cite{Kabanets-Impagliazzo}};}
    }
    \onslide<3->{
      \draw[->, bend left, ultra thick ,onslide=<4->{black!20,thin}] (LB) to (HS);
      \onslide<3>{\node[anchor=east] at (5,-2.5) {\Cite{Heintz-Schnorr, Agrawal}};}
    }
  \end{tikzpicture}

\end{frame}


\begin{frame}
  \frametitle{How are hitting sets constructed?}

  \only<1-5>{
    \begin{center}
      \begin{tikzpicture}[transform shape, scale=0.8]
        \draw[thick] (5,-1) -- (2,-3) -- (8,-3) -- cycle;
        \node at (5,-2) {$C$};
        \node at (5,-0.1) {Nonzero}
        edge[<-,thick] (5,-1);
        \foreach \x in {2.5,3,...,7.5}
        {\draw[<-,thick] (\x,-3) -- ++(0,-0.5);}
        \only<1,3->{
          \node at (2.5,-3.7) {\small \only<1>{$x_1$}\only<3->{$g_1$}};
          \node at (3,-3.7) {\small \only<1>{$x_2$}\only<3->{$g_2$}};
          \node at (3.5,-3.7) {\small \only<1>{$x_3$}\only<3->{$g_3$}};
          \node at (5.5,-3.7) {\huge $\cdots$};
          \node at (7.5,-3.7) {\small \only<1>{$x_n$}\only<3->{$g_n$}};
        }
        
        \only<2>{
          \draw[thick] (2,-3.5) -- (4,-5) -- (6,-5) -- (8,-3.5) --  cycle;
          \node at (5,-4.25) {Variable Reduction};
          \foreach \x in {4.3,4.6,...,5.8}
          {\draw[<-,thick] (\x,-5) -- ++(0,-0.5);}
          \node at (4.3,-5.7) {$y_1$};
          \node at (5,-5.7) {$\cdots$};
          
          \node at (5.6,-5.7) {$y_\ell$};
        }
      \end{tikzpicture}
    \end{center}

    \vskip -1em
  }
  \only<4->{
    \begin{definition}[Generator]
      A map $\mathcal{G} = (g_1,\ldots, g_n) \in \F[y_1,\ldots, y_\ell]^n$ is a \Emph{hitting-set generator} for a class $\mathcal{C}$ if
      \[
        \forall\; C \in \mathcal{C}\quad,\quad C \neq 0 \Longleftrightarrow C\circ \mathcal{G} \neq 0.
      \]
      \only<5->{
        \noindent
        \!The \Emph{degree} of the generator is $\max_i (\deg g_i)$. The \Emph{stretch} is $\ell \rightarrow n$.

        }
    \end{definition}
  }
  \only<7->{

    \vskip 2em
    
    \begin{lemma}
      Let $\mathcal{G} = (g_1,\ldots, g_n) \in \F[y_1,\ldots, y_\ell]^n$ be an explicit hitting-set generator for $\mathcal{C}(n,D,s)$ of degree $d$. Then, we have
      \begin{itemize}
      \item An explicit hitting set $H$ of size $(dD + 1)^\ell$

      \end{itemize}
    \end{lemma}
  }
  
\end{frame}

\begin{frame}
  \frametitle{Generators assuming hardness}

  \vskip 2em
  
  \begin{tabularx}{\textwidth}{l|l|l}
    & Hardness assumption & Hitting set size\\
    \hline
    & & \\
    \onslide<2->{    \multirow{3}{*}{\Cite{Kabanets-Impagliazzo}} & $\set{p_n}$ requires $n^{\omega(1)}$ size & $2^{s^{\epsilon}}$,  $\forall\;\epsilon > 0$\\
      & $\set{p_n}$ requires $2^{n^{\Omega(1)}}$ size & $2^{\poly\log s}$\\
      & $\set{p_n}$ requires $2^{\Omega(n)}$ size & $s^{O(\log s)}$\\
      & & \\
      \hline
      & & \\}
    \onslide<3->{
    \Cite{Kumar-S-Tengse} & $\set{p_{k,d}}_d$ requires $d^{\Omega(1)}$ size & $s^{\exp(\exp(\log^\ast s))}$\\
    & & \\
    \hline
    & & \\}
  \onslide<4->{
    \only<5->{\Emph{This work}}& \only<4>{???}\only<5->{$\set{p_{k,d}}_d$ requires $d^{3 + \epsilon}$ size} & $s^{O(1)}$\\
    & & \\
    & \only<6->{$\set{p_{k,d}}_d$ requires $d^{1 + \epsilon}$ $\overline{\text{size}}$}  & \only<6->{$s^{O(1)}$}
  }
  \end{tabularx}
\end{frame}

\section{Our results}

\begin{frame}[c]
  \begin{center}
    
    {\fontsize{25}{30}\frametitlefont \textcolor{Primary1}{Our results}}

  \end{center}

\end{frame}

\begin{frame}
  \frametitle{Main Theorem}

  \begin{theorem}[\Cite{Guo-Kumar-S-Solomon}]
    For any $k$-variate polynomial $P$ of degree $d$, \only<2->{ there is an explicit map
      \[
        \mathcal{G}_P = (g_1,\ldots, g_n) \in \F[y_1,\ldots, y_{k},z_1,\ldots, z_k]^n
      \]
      such that}\only<3->{
      \begin{itemize}
      \item $\deg(\mathcal{G}_P) = d$ and $\mathcal{G}_P$ is $d^{O(k)}$-explicit,\only<4->{
        \item For any nonzero circuit $C \in \mathcal{C}(n,D,s)$,
          \[
            \text{if }C \circ \mathcal{G}_P = 0\quad,\quad\text{then } \only<4-7>{\operatorname{size}}\only<8>{\;\Emph{\overline{\operatorname{size}}}}\,(P) \only<5->{\leq n^{10k} \cdot s \cdot \alert<8>{d}\only<4-7>{{}^3} \cdot D}\only<4,6->{\ll d^k}
          \]
          \only<6->{\Gray{(Think of $d = n^{1000}$)}}
        }
      \end{itemize}
    }

    \vskip 1em
    \only<7->{
    In other words, if $P$ is \Emph{hard enough}, then $\mathcal{G}_P$ is a hitting-set generator for $\mathcal{C}(n,D,s)$.}
  \end{theorem}
\end{frame}

\begin{frame}
  \frametitle{Some consequences}

  \begin{corollary}
    Let $k$ be a large enough constant and $\epsilon > 0$. Suppose $\set{P_{k,d}}_d$ is an explicit family of polynomials with $\deg P_{k,d} = d$ such that

    $\set{P_{k,d}}_d$ requires size $d^{3+\epsilon}$ \Gray{\scriptsize (or $\overline{\text{size}}$ $d^{1+\epsilon})$}.

    \vskip 1em
    
    Then, there is an explicit hitting set for $\mathcal{C}(s,s,s)$ of size $\poly(s)$. 
  \end{corollary}


  \onslide<2->{
    \begin{proof}
      Set $d \geq s^{(10k+2)/\epsilon}$ and $P = P_{k,d}$. 
      
      \onslide<3->{If $0\neq C \in \mathcal{C}(s,s,s)$ such that $C \circ \mathcal{G}_P = 0$,}\onslide<4->{ then
        \begin{align*}
          \operatorname{size}(P) & \leq s^{10k} \cdot s^2 \cdot d^3\onslide<5->{\\
                                 & \leq d^{3+\epsilon}}\onslide<6->{\text{ which is impossible.}}
        \end{align*}
      }
      \onslide<7->{Hence $C \circ \mathcal{G}_P$ is a nonzero $2k$-variate polynomial of degree at most $ds$.}\onslide<8->{ Hence, we have a hitting set of size $(ds)^{2k} = s^{O(k^2/\epsilon)}$. }
    \end{proof}
  }
\end{frame}

\begin{frame}
  \frametitle{Revisiting variable reductions}

      \begin{center}
      \begin{tikzpicture}[transform shape, scale=0.8]
        \draw[thick] (5,-1) -- (2,-3) -- (8,-3) -- cycle;
        \node at (5,-2) {$C$};
        \node at (5,-0.1) {}
        edge[<-,thick] (5,-1);
        \foreach \x in {2.5,3,...,7.5}
        {\draw[<-,thick] (\x,-3) -- ++(0,-0.5);}

        % \node at (2.5,-3.7) {\small \only<1>{$x_1$}\only<3->{$g_1$}};
        % \node at (3,-3.7) {\small \only<1>{$x_2$}\only<3->{$g_2$}};
        % \node at (3.5,-3.7) {\small \only<1>{$x_3$}\only<3->{$g_3$}};
        % \node at (5.5,-3.7) {\huge $\cdots$};
        % \node at (7.5,-3.7) {\small \only<1>{$x_n$}\only<3->{$g_n$}};
        
        \draw[thick] (2,-3.5) -- (4,-5) -- (6,-5) -- (8,-3.5) --  cycle;
        \node at (5,-4.25) {Variable Reduction};
        \foreach \x in {4.3,4.6,...,5.8}
        {\draw[<-,thick] (\x,-5) -- ++(0,-0.5);}
        \node at (4.3,-5.7) {$y_1$};
        \node at (5,-5.7) {$\cdots$};
        
        \node at (5.6,-5.7) {$y_\ell$};
        
      \end{tikzpicture}
    \end{center}

    \only<2->{
      \Emph{\bf Hitting-set Generator:} $C \neq 0\quad\Longleftrightarrow \quad C \circ \mathcal{G} \neq 0$
    }

    \only<3->{

      \vskip 1em
      
      \Emph{\bf Dream:} $\quad\quad\quad\quad\operatorname{size}(C \circ \mathcal{G}) \quad\;\;\approx\;\;\quad \operatorname{size}(C) + \operatorname{size}(\mathcal{G})$
    }

\end{frame}


\begin{frame}
  \frametitle{The Kronecker Map}

  \only<2->{
    \[
      \mathcal{K}\only<5->{_t} = \only<2-4>{\inparen{1,y,y^2, y^4, \ldots, y^{2^{n-1}}}}\only<5->{\inparen{1,y_1,y_1^2, \ldots, y_1^{2^{m-1}},\ldots, 1,y_t,\ldots, y_t^{2^{m-1}}} \quad{\small (n = tm)}}
    \]
  }
  \only<3->{
    \[
      x_1^{e_1}\cdots x_n^{e_n} \quad\mapsto\quad \only<3-4>{y^{[e_1e_2\cdots e_n]_2}}\only<5->{y_1^{[e_1\cdots e_m]_2} \cdots y_t^{[e_*\cdots e_n]_2} }
    \]
    
    \vskip 1em
  }
  \only<4->{
    If $P$ is a $n$-variate multilinear polynomial, then
    
    $P \circ \mathcal{K}$ is a \only<4>{univariate}\only<5->{$t$-variate} polynomial of degree at most \only<4>{$2^n$}\only<5->{$2^{n/t}$}. 
  }
  \only<6->{

    \vskip 1.5em
    
    \Cite{Kabanets-Impagliazzo}: If $\set{P_n}$, multilinear, with $\operatorname{size}(P_n) > 2^{n/1000}$,
    
    then we have $s^{O(\log s)}$-sized hitting sets. 
  }
  \only<7->{

    \vskip 1.5em
    
    \Emph{\bf New}: If, for some constant $t$, suppose $\overline{\operatorname{size}}(P_n \circ \mathcal{K}_t) \geq 2^{(1+\epsilon) n/t} \only<8->{ \Gray{= d^{1+\epsilon}}}$
    
    \only<9->{then we have $\poly(s)$-sized hitting sets.}
  }

\end{frame}

\begin{frame}
  \frametitle{Consequences for bootstrapping}

  \vskip 2em
  
  \begin{block}{\only<1>{Theorem. \Cite{Kumar-S-Tengse}}\only<2>{Corollary}}
    Let $\epsilon > 0$ and $k$ (large enough) be fixed constants.

    \vskip 1em
    
    If, for all $s \geq k$, we have explicit hitting sets for \only<1>{$\mathcal{C}(k,s,s)$}\alert<2>{\only<2->{$\overline{\mathcal{C}}(k,s,s)$}} of size
    \[
      s^{k-\epsilon},
    \]
    then, we have explicit hitting sets for \only<1>{$\mathcal{C}(s,s,s)$}\alert<2>{\only<2->{$\overline{\mathcal{C}}(s,s,s)$}} of size
    \[
      s^{\only<1>{\exp(\exp(\log^\ast s))}\alert<2>{\only<2->{O(1)}}}
    \]
  \end{block}
  \only<2>{\footnotesize \hfill \Gray{Circuits and border are crucial for this.}}
  
\end{frame}

\section{Border shit}

\begin{frame}[c]
  \begin{center}
    
    {\fontsize{25}{30}\frametitlefont \textcolor{Primary1}{What's all this}}

    \vspace{0.4cm}

    {\fontsize{25}{30}\frametitlefont \textcolor{Primary1}{border stuff?}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{The Border}

  \begin{center}
    \begin{tikzpicture}[transform shape, scale=0.8]
      \draw[fill=cyan!10, draw=black] (-4,-3) rectangle (4,3);
      \node[anchor=east] at (4,3.5) {\footnotesize All polynomials};

      \only<2->{
        \draw[fill=cyan!40,thick, draw=black,onslide=<3->{dashed}] (0,0) ellipse (3cm and 2cm);
        \node at (0,0) {\footnotesize Polynomials with size $s$ circuits};
      }
      \only<4->{
        \node[draw, fill=blue, circle, inner sep=2pt] at (45: 3cm and 2cm) {};
      }
      \onslide<5->{
        \node[draw, fill=blue, circle, inner sep=2pt] at (-3.8,-3.5) {};
        \node[anchor=west] at (-3.5,-3.5) {\scriptsize \Emph{Does not} have size $s$ circuits, but \Emph{arbitrarily close} to those that do.};
        }
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Border computation: an example}

  \[
    \mathcal{C} = \setdef{f}{f = \ell_1^d +  \ell_2^d\;\;,\;\;\deg(\ell_1),\deg(\ell_2) = 1}
  \]
  \only<2->{
    \begin{block}{Fact}
      If $x^{d-1}y = \ell_1^d + \cdots +\ell_s^d$, then $s \geq d$.
      
      \only<3->{Hence, $x^{d-1}y \notin \mathcal{C}$ for any $d \geq 3$. }
    \end{block}
  }
  
  \vskip 1em
  
  \only<4->{However,}
  \onslide<4->{
    \begin{align*}
      \onslide<4->{C & = \frac{(x+\epsilon y)^d - x^d}{d\cdot \epsilon}}
                       \onslide<5->{ = x^{d-1}y + O(\epsilon)}
                       \onslide<6->{ \;\;\stackrel{\epsilon\rightarrow 0}{\longrightarrow} \;\;x^{d-1}y}
    \end{align*}
  }
  
  \onslide<7->{Hence, $x^{d-1}y \in \overline{\mathcal{C}}$ but not in $\mathcal{C}$.}
\end{frame}

\begin{frame}
  \frametitle{The one trick that we will need}

  \vskip 1.5em

  \only<2->{
    {\bf Task:} Given a circuit $C$ of size $s$ computing a polynomial $P$ of degree $d$. Compute $P_d$, the degree $d$ homogeneous part of $P$. 
  }

  \only<3->{
    \vskip 1em

    {\bf Standard solution:} \Emph{``Homogenize''} the circuit and extract the degree $d$ part. Can be done using a circuit of size $O(s d^2)$. 
  }

  \only<4->{
    \vskip 1.5em

    {\bf Border trick:}
    \begin{align*}
      \only<6->{\epsilon^d \cdot} C\inparen{\only<4>{x_1}\only<5->{\frac{x_1}{\epsilon}},\ldots, \only<4>{x_n}\only<5->{\frac{x_n}{\epsilon}}}  & \;\;=\;\; \only<6->{\epsilon^d}P_0 + \only<4>{P_1}\only<5>{\frac{P_1}{\epsilon}}\only<6->{\epsilon^{d-1}P_1} + \cdots + \only<4,6->{P_d}\only<5>{\frac{P_d}{\epsilon^d}}\only<7->{\\
      & \stackrel{\epsilon \rightarrow 0}{\longrightarrow} P_d
        }\only<8->{\\
      \therefore \quad\overline{\operatorname{size}}(P_d) & \;\;\leq\;\;\overline{\operatorname{size}}(P)
      }
    \end{align*}
  }
\only<9->{$P_d$ can be $\overline{\text{computed}}$ in size $s$ as well!}

\end{frame}

\begin{frame}[c]
  \begin{center}
    
    {\fontsize{35}{40}\texttt{\textcolor{Primary1}{\textbackslash begin\{}\textcolor{Primary2}{proof}\textcolor{Primary1}{\}}}}

  \end{center}

\end{frame}

\begin{frame}
  \frametitle{Designing generators}

  \vskip 3em

  \begin{center}
    Any sufficiently \only<1>{advanced}\only<2->{\Emph{hard polynomial's \only<2-5>{evaluations}\only<6>{\Emph{components}}}}
    \vskip 2em
    
    \only<1>{technology}\only<2-3>{\Emph{on disjoint inputs}}\only<4,5>{\Emph{on {\bf ``almost disjoint''} inputs}}\only<6>{\Emph{`Taylored' appropriately}}
    
    \vskip 2em
    
    is indistinguishable\only<2->{, \Emph{for a small circuit,}}

    \vskip 2em
    
    from \only<1>{magic}\only<2->{\Emph{random inputs}}
  \end{center}

  \only<3-4>{
    \[
      \mathcal{G}: (\vecy_1,\ldots, \vecy_k) \mapsto (\vecy_1, \ldots, \vecy_k, P(\vecy_1),\ldots, P(\vecy_k))
    \]
  }
  \only<5>{
    \[
    \text{\Cite{KI, NW}}:\quad  \mathcal{G}: (y_1,\ldots, y_\ell) \mapsto \inparen{P(\vecy\mid_{S_1}),\ldots, P(\vecy\mid_{S_n})}
    \]
    }
\end{frame}

\begin{frame}
  \frametitle{Description of our generator}
  \only<1>{
    \[ \hfill P(z_1,\ldots, z_k)\hfill \]
  }
  \only<2->{\begin{align*}
    P(\vecy + \vecz) & = \alert<5>{P(\vecz)} + \alert<6>{\sum_i y_i \cdot (\partial_{i}P)(\vecz)} + \alert<7>{\sum_{i,j} y_i y_j \cdot (\partial_{i,j}P)(\vecz)} + \cdots\only<3->{\\
                     & = \sum_{\vece} \frac{\vecy^{\vece}}{\vece!} \cdot (\partial_{\vece}P)(\vecz)}
            \end{align*}

            \vspace{-1em}

          }
  \only<4->{
    \begin{definition}[The generator]
      For a $k$-variate polynomial $P$, define
      \[
        \Delta_i(P) = \sum_{\vece: \abs{\vece} = i} \frac{\vecy^{\vece}}{\vece!} \cdot (\partial_\vece P)(\vecz).
      \]
      The generator $\mathcal{G}_P$ is defined as
      \[
        \mathcal{G}_P = (\alert<5>{\Delta_0(P)}, \alert<6>{\Delta_1(P)},\alert<7>{\Delta_2(P)},\ldots, \Delta_n(P)) \in (\F[\vecy_{[k]},\vecz_{[k]}])^{n+1}.
      \]
    \end{definition}
  }
\end{frame}


\begin{frame}
  \frametitle{Proof overview}

  \onslide<2->{
    \begin{itemize}
       \item Assume $C\neq 0$ is a small circuit such that $C \circ \mathcal{G}_P = 0$.
        
        \vskip 6em

      \onslide<3->{      
      \item Show that we can use $C$, and a little more, to get a circuit that computes $P$.
      }

      \onslide<4->{

        \begin{quote}

          \vskip 2em
          
          {\bf \Emph{Idea:} }Think of $C(\Delta_0(P),\ldots, \Delta_n(P)) = 0$ as a

          \Emph{differential equation} and solve for $P$.

      \end{quote}
      
      }
    \end{itemize}
  }
\end{frame}

\begin{frame}
  \frametitle{\only<1-9>{Cauchy-Kowalevski Equations}\only<10->{Our situation}}

  \[
    \only<1-3>{\pfrac{1}{2} m\cdot \inparen{\only<1,2>{\alert<2>{v(t)}}\only<3->{\alert<3>{\frac{\partial h}{\partial t}}}}^2 + m\cdot g\cdot h(t) = c}\only<4-10>{\hskip 2cm Q(h(t),h^{(1)}(t)) = 0}\only<11->{\hskip 2cm \alert<11>{C(\Delta_0(P),\ldots, \Delta_n(P)) = 0}}
  \]

  \onslide<5->{
    \vspace{1em}

    Solve for \only<5-11>{$h(t)$}\only<12->{\alert<12>{$P$}} as a \alert<5>{power series} in \only<5-11>{$t$}\only<12->{\alert<12>{$\vecz$}}.
    
    
    \only<6->{
      \begin{itemize}
      \item 
        Start with some \alert<6>{non-degenerate} initial conditions:
        \only<1-12>{\[
            t = a_0\quad;\quad h(a_0) = \beta_0\quad;\quad h'(a_0) = \gamma_0
          \]}
        \only<13->{
          \alert<13>{
            \begin{align*}
              C \circ \mathcal{G}_P & = 0\\
              (\partial_n C) \circ \mathcal{G}_P & \neq 0.
            \end{align*}
          }
        }
        \only<7-12>{
          which is a solution modulo $(t - t_0)$.
          
          \vskip 2em
        }
        
        \only<8->{
          
        \item \only<8-13>{Lift to a solution modulo $(t - t_0)^2$, $(t-t_0)^3$ and so on...\only<9->{

              \Emph{Newton Iterations}}}
          \only<14->{\alert<14>{Compute the homogeneous parts of $P$, one by one, via Newton Iteration}}
        }
      \end{itemize}
    }
  }
\end{frame}

\begin{frame}
  \frametitle{Setting-up the initial conditions}

  \vspace*{-1em}
  
  \begin{center}
    {\tiny \Gray{(Assuming that $\mathcal{G}_P$ is \emph{not} a generator)}}
  \end{center}

  \onslide<2->{
    { 
      {\bf Goal:} Find a circuit $C'$ of small size such that
      { \begin{align*}
          C' \circ \mathcal{G}_P & = 0\\
          (\partial_n C') \circ \mathcal{G}_P & \neq 0.
        \end{align*}}
    }
  }
  \onslide<3->{

    \vspace{-2em}
    \begin{tikzpicture}
      \helpgrid{10}{5}
      \node[anchor=south east] (top) at (7, 4) {\only<6->{If }\only<5->{$\tilde{C}(x_n) = $} $C(\only<3>{x_0}\only<4->{g_0},\ldots, \only<3>{x_{n-1}}\only<4->{g_{n-1}},\only<3,5->{x_n}\only<4>{g_n}) \only<3,10->{\neq}\only<4,6-9>{=}\only<5>{\stackrel{?}{=}} 0$};
      \only<7-9>{
        \only<7->{
          \node at (2.5,3) {$C(x_0,\ldots, x_{n-1},a) \neq 0$};
        }
        \only<8->{
          \node at (2.5,2.5) {$C(g_0,\ldots, g_{n-1},a) = 0$};
        }
        \only<9>{
          \node at (2.5,1.5) {\footnotesize Contradicts minimality!};
        }
      }
      \only<11-16>{
        \only<11->{
          \node at (2.5,3) {$C(g_0,\ldots, g_{n-1},g_n) = 0$};
        }
        \only<13->{
          \node at (2.5,2.5) {$(\partial_n C) (g_0,\ldots, g_n) = 0$};
        }
        \only<14->{
          \node at (2.5,2) {$(\partial_n^2 C) (g_0,\ldots, g_n) = 0$};
        }
        \only<15->{
          \node at (2.5,1.5) {$\vdots$};
          \node at (2.5,1) {$(\partial_n^r C) (g_0,\ldots, g_n) = 0$};
        }
      }
      \only<17->{
        \node at (5,3) {$(\partial_n^rC)(g_0,\ldots, g_{n-1},g_n) = 0$};
        \node at (5,2.5) {$(\partial_n^{r+1}C)(g_0,\ldots, g_{n-1},g_n) \neq 0$};          
      }
      \only<18->{
        \node at (5,1.5) {$C' = (\partial_n^{r}C)$ is what we want.};          
      }
      \only<19->{
        \node at (5,0.75) {And, $\operatorname{size}(C')  \leq \operatorname{size}(C) \cdot \deg (C)$};          
      }
      \only<12-16>{
        \only<12->{
          \node at (8,3) {$(x_n - g_n)\only<13>{{}^2}\only<14>{{}^3}\only<15->{{}^{r+1}}$ divides $\tilde{C}$};
        }
        \only<16->{
          \node at (8,1.5) {$(x_n - g_n)^t$ cannot divide $\tilde{C}$};
          \node at (8,1) {if $t > \deg \tilde{C}$};
        }
      }
    \end{tikzpicture}
  }
\end{frame}


\begin{frame}
  \frametitle{Some basic properties}

  \[
    \Delta_i(P) = \sum_{\vece: \abs{\vece} = i} \frac{\vecy^{\vece}}{\vece!} \cdot (\partial_\vece P)(\vecz).
  \]

  \onslide<2->{
    \begin{tikzpicture}
      \helpgrid{11}{5}
      \node[anchor=west] at (0.5,4.5) {\bf \Emph{Additivity:}};
      \node[anchor=west] at (1,4) {$\Delta_i(P + Q) = \Delta_i(P) + \Delta_i(Q)$};

      \only<3->{
        \node[anchor=west] at (0.5,3) {\bf \Emph{`Homogeneity':}};
        \node[anchor=north west] at (1.05,2.5) {
          $P(\vecz) = Q(\vecz) \bmod{\inangle{\vecz}^t}$
        };
        \node[anchor=north west] at (0,2) {
          $\implies \Delta_i(P) = \Delta_i(Q) \bmod{\inangle{\vecz}^{t-i}}$
        };
      }
      \only<4->{
        \draw [decorate,decoration={brace,amplitude=10pt,raise=4pt},ultra thick,yshift=0pt, draw=Primary1]
        (5.5,4.7) -- (5.5,1);
        \node[anchor=west] at (6.65,3.5) {$P = P_0 + \cdots + P_d$};
        \node[anchor=west] at (6,2.5) {$\Delta_i(P) = \Delta_i(P_{\leq t+i-1}) \bmod{\inangle{\vecz}^t}$};
      }
    \end{tikzpicture}
  }
\end{frame}

\begin{frame}
  \frametitle{The Reconstruction Step}
  \begin{tikzpicture}
    \node[anchor=east] at (3.4,6.5) {$C' \circ \mathcal{G}_P\;(\vecy,\only<1>{\vecz}\only<2->{\alert<2,3>{\mathbf{0}}}) = 0$};
    \node[anchor=east] at (3.4,6) {$(\partial_nC') \circ \mathcal{G}_P\;(\vecy,\only<1>{\vecz}\only<2->{\alert<2,3>{\mathbf{0}}}) \neq 0$};
    \onslide<3>{\node[anchor=north west,text=black!50,text width=5cm, align=left] at (5,7) {Else, replace $\inangle{z_1,\ldots, z_\ell}$ with $\inangle{z_1-\alpha_1,\ldots, z_k-\alpha_k}$ in what follows};}

    \only<5->{
      \node[anchor=west] at (1,5) {$P = P_0 + \cdots + P_n + \alert<8-28>{P_{n+1}}+ \cdots+ P_d$};
    }
    \only<6,7>{
      \draw [decorate,decoration={brace,amplitude=10pt,raise=4pt,mirror}, thick,yshift=0pt, draw=Primary1]
      (1.8,4.8) -- (3.5,4.8) node [black,midway,yshift=-0.4cm,anchor=north,align=center] {\footnotesize Bruteforce\\\footnotesize in $n^{O(k)}$ size};
    }
    \only<7>{
      \draw [decorate,decoration={brace,amplitude=10pt,raise=4pt,mirror}, thick,yshift=0pt, draw=Primary1]
      (4,4.8) -- (6.2,4.8) node [black,midway,yshift=-0.4cm,anchor=north,align=center] {\footnotesize Compute, via\\\footnotesize Newton iterations,\\\footnotesize one by one};
    }
    \only<9-20>{
      \only<12->{
        \node[anchor=west,text=black!50] at (6.8,4.2) {\footnotesize ($\Delta_i(P) = \Delta_i(P_{\leq t+i-1}) \bmod{\inangle{\vecz}^t}$)};
      }
      \node[anchor=west] at (1,3.5) {$C'\inparen{\only<9>{g_0}\only<10-12>{\Delta_0(P)}\only<13->{\Delta_0(P_{\leq n})},\ldots, \only<9>{g_{n-1}}\only<10-12>{\Delta_{n-1}(P)}\only<13->{\Delta_{n-1}(P_{\leq n})},\only<9>{g_n}\only<10-12>{\Delta_n(P)}\only<13>{\Delta_n(P_{\leq n+1})}\only<14->{\Delta_n(P_{\leq n}) + \alert<15>{\Delta_n(P_{n+1})}}} = 0\only<11->{\bmod{\inangle{\vecz}^2}}$};
      \only<16->{
        \node[anchor=west] at (1,2.8) {$C'(R_0,\ldots, R_{n-1},R_n + A) = 0\bmod{\inangle{\vecz}^2}$};
      }
      \only<17->{
        \node[anchor=west] at (1,2) {$ = C'(R_0,\ldots, R_{n-1},R_n) + A \cdot \inparen{\only<17,18>{(\partial_n C')(R_0,\ldots, R_n)\only<18>{(\vecy,\mathbf{0})}}\only<19->{(\partial_nC')\circ \mathcal{G}_P\;(\vecy,\mathbf{0})}} = 0\bmod{\inangle{\vecz}^2}$};
      }
      \only<20>{
        \node[anchor=west] at (1,1) {$\therefore A = \pfrac{C'(R_0,\ldots, R_n)}{(\partial_n C') \circ \mathcal{G}_P\;(\vecy,\mathbf{0})} \bmod{\inparen{\vecz}^2}$};
        }
      }
    \end{tikzpicture}
    \only<21->{

      \vskip 1em
      
      \fbox{
      \[ \hspace{-0.7cm} \Delta_n(P_{\alert<29>{n+\only<29>{j+}1}})\only<22->{(\veca,\vecz)} = \pfrac{C'\inparen{\Delta_0(P_{\alert<29>{\leq n \only<29>{+j}}}),\ldots, \Delta_n(P_{\alert<29>{\leq n\only<29>{+j}}})}\only<22->{(\veca,\vecz)}}{(\partial_n C') \circ \mathcal{G}_P\;(\only<21>{\vecy}\only<22->{\veca},\mathbf{0})} \bmod{\inangle{\vecz}^{\alert<29>{\only<29>{j+}2}}}
      \]
      }
  }

    \only<23->{
      \begin{center}
        By trying many $\veca$'s, we can obtain all of ${\partial}^{=n}(P_{\alert<29>{n+\only<29>{j+}1}})$

        \only<24->{
          and hence $P_{\alert<29>{n+\only<29>{j+}1}}$ itself
        }

        \only<27->{\alert<27>{\only<27>{modulo higher order junk}\only<28->{\st{modulo higher order junk}}}}

        \only<28->{
          \vspace{2em}
          
          \Emph{\Large Border tricks!}

          \Gray{Or careful homogenisation}
        }
        
        \only<25>{

          \vskip 2em

          \footnotesize\Gray{(Euler formula: $d\cdot f = \sum x_i \partial_{i} f$, if $f$ homogeneous of degree $d$)}}
      \end{center}
      }
    
    % \only<23->{
    %   \node at (5.5,2) {By trying many $\veca$'s, we can obtain all of ${ \partial}^{=n}(P_{n+1})$};
    %   \only<24->{\node at (5.5,1.5) {and hence $P_{n+1}$ itself};}
    %   \only<27->{\node at (5.5,1) {\alert<27>{modulo higher order junk}};}
    %   \only<28->{\node at (5.5,0) {\Emph{Border tricks!}};
    %     \node at (5.5,-0.5) {\footnotesize\Gray{Or careful homogenisation}};
    %   }
      
    %   \only<25>{\node[text=black!50] at (5.5,0.5) {\footnotesize(Euler formula: $d\cdot f = \sum x_i \partial_{i} f$, if $f$ homogeneous of degree $d$)};}
    % }
\end{frame}

\begin{frame}
  \frametitle{Reconstruction Step: Pictorially}

  \vspace{0.5em}
  
  \begin{tikzpicture}[transform shape, scale=0.9,onslide=<7->{draw=Red}]
    \onslide<1->{
      \draw[<-] (-1,-1 ) -- +(0,-0.3);
      \node at (-1,-1.5) {{\small $z_1$}};
      \draw[<-] (-0.6,-1 ) -- +(0,-0.3);
      \node at (-0.6,-1.5) {{\small $z_2$}};
      \draw[<-] (1,-1 ) -- +(0,-0.3);
      \node at (1,-1.5) {{\small $z_m$}};
      \node at (0.3,-1.2) {$\cdots$};
      
      
      \draw[thick] (-1,-1) -- ++(-1,1) -- ++(4,0) -- ++(-1,-1) -- cycle;
      \node at (0,-0.5) {$\only<1-13>{B_{0}}\only<14->{B_{j}}$};
      \onslide<1>{
        \foreach\x in {-2,-1.6,...,2} \draw[->] (\x,0) -- ++(0,0.25);
      
        \node at (0,0.75) {\small $\displaystyle\setdef{\partial_{\vecz^{\vece}}(P_{i})}{\abs{\vece} \leq n\;,\;i \leq n}$};
        \draw[dashed] (-2,0.25) rectangle (2,1.25);

      }
    }
    
    \onslide<2,3->{
      \draw[thick] (-2.1,0.1) -- ++(-0.9,0.9) -- ++(6,0) -- ++(-0.9,-0.9) -- cycle;
      \node at (0, 0.5) {\small Linear combinations};
      
      \draw[->] (-3,1 ) -- +(0,0.3);
      \draw[->] (-2.7,1 ) -- +(0,0.3);
      \draw[->] (-1,1 ) -- +(0,0.3);
      \node at (-1.8,1.2) {$\cdots$};
      \draw[dotted,black!70] (-3,1.4) rectangle (-1,2);
      \node at (-2,1.7) {$\only<1-13>{\Gamma_{\veca_1}}\only<14->{\Gamma_{j,\veca_1}}$};
      \draw[->] (1,1 ) -- +(0,0.3);
      \draw[->] (1.3,1 ) -- +(0,0.3);
      \draw[->] (3,1 ) -- +(0,0.3);
      \node at (2.2,1.2) {$\cdots$};
      \draw[dotted,black!70] (1,1.4) rectangle (3,2);
      \node at (2,1.7) {$\only<1-13>{\Gamma_{\veca_N}}\only<14->{\Gamma_{j,\veca_N}}$};
      \node at (0,1.7) {$\cdots$};

      \onslide<3>{
        \node at (0,3.5) {$\Gamma_{\veca} = \inparen{\Delta_0(P_{\leq n})(\veca,\mathbf{z})\;,\; \ldots \;,\; \Delta_n(P_{\leq n})(\veca,\mathbf{z})}$};
      }
    }
    \onslide<4,5->{
      \draw[thick] (-3,2.1) -- ++(2,0) -- ++(-1,0.9) -- cycle;
      \node at (-2,2.5) {\small $C'$};
      \draw[->] (-2,3) -- ++(0,0.5);
      \node at (-2.4,3.25) {\tiny $\only<1-13>{\beta_1}\only<14->{\beta_{j,1}}$};
      
      \draw[thick] (1,2.1) -- ++(2,0) -- ++(-1,0.9) -- cycle;
      \node at (2,2.5) {\small $C'$};
      \draw[->] (2,3) -- ++(0,0.5);
      \node at (2.4,3.25) {\tiny $\only<1-13>{\beta_N}\only<14->{\beta_{j,N}}$};
      \onslide<5>{
        \node at (-2,3.8) {\small $\Delta_n(P_{n+1})(\veca_1,\mathbf{z}) + \text{junk}$};
        \node at (2,3.8) {\small $\Delta_n(P_{n+1})(\veca_N,\mathbf{z}) + \text{junk}$};

      }
    }
    \onslide<6->{
      \node at (0,2.5) {$\cdots$};
      \draw[thick] (-2.2,3.5) -- ++(-1,1) -- ++(6.4,0) -- ++(-1,-1) -- cycle;
      \node at (0,4) {\footnotesize Linear combinations + Euler};
      
      \foreach\x in {-3,-2.7,-2.4,...,-1} \draw[->] (\x,4.5) -- ++(0,0.3);
      \foreach\x in {1,1.3,1.6,...,3} \draw[->] (\x,4.5) -- ++(0,0.3);
      \node at (0,4.6) {$\cdots$};
      \draw[dashed] (-3.15,4.8) rectangle (3,5.8);
      \only<6,7>{
        \node at (0,5.3) {\small $\setdef{\partial_{\vecz^{\vece}}(P_{n+1}) \only<6>{+ \text{junk}}}{\abs{\vece} \leq n}$};
      }
      \onslide<8->{
        \node at (0,5.3) {\small $\displaystyle\setdef{\partial_{\vecz^{\vece}}(P_{i})}{\abs{\vece} \leq n\;,\;i \leq n+\only<1-13>{1}\only<14->{j+1}}$};
        \draw[->, thick] (0,0) -- (0,0.05) -- (3.5,0.05) -- (3.5,5.3) -- (3.15,5.3); ;
      }
    }
    \onslide<9,15->{
      \draw[fill=Primary2!50, opacity=0.9] (-3.7,4.55) rectangle (3.7,-1.05);
      \node at (0,1.75) {\Large $\only<9>{B_1}\only<15->{B_{j+1}}$};
    }
    \onslide<10->{
      \only<10->{
        \draw[draw=black,thick] (5,-1) -- ++(2,0);
        \node at (6,-1.5) {$\only<13>{s_{1}}\only<14->{s_{j+1}}$};
        \node at (6,-0.5) {$\only<10-13>{s_0= n^{O(k)}}\only<14->{s_j}$};
      }
      \only<11->{
        \node at (6,0.5) {$n^{O(k)}$};
      }
      \only<12->{
        \node at (6,2.5) {$s' \cdot n^{O(k)}$};
      }
      \only<13->{
        \node at (6,4) {$n^{O(k)}$};
      }
      \onslide<16->{
        \node at (2.5,-2.5) {\large $s_d \quad\leq\quad \only<16>{s'}\only<17->{s \cdot D} \cdot n^{O(k)} \cdot d$};
      }
    }
  \end{tikzpicture}

\end{frame}



\begin{frame}[c]
  \begin{center}
    
    {\fontsize{35}{40}\texttt{\textcolor{Primary1}{\textbackslash end\{}\textcolor{Primary2}{proof}\textcolor{Primary1}{\}}}}

  \end{center}

\end{frame}

\section{Conclusions}

\begin{frame}[label=conclusion]

  \frametitle{Conclusion}
  \pause
  {\Large \Emph{\bf Summary:}}
  
  \begin{itemize}
  \item With suitable hardness, we can get poly-sized hitting sets.  \pause
  \item With the \Emph{border}, we can bootstrap from barely non-trivial hitting sets.
  \end{itemize}
  \pause
  
  \vskip 2em
  
  {\Large \Emph{\bf Open Problems:}}
  \begin{itemize}
  \item Current proof requires \Emph{characteristic zero fields}. Ought to work for all fields.  \pause
  \item The hardness depends on the \Emph{degree} of the circuit we are fooling. Ought to fool all small size circuits \Emph{irrespective of degree} \Gray{(using the border)}.   \pause
  \end{itemize}

  \begin{center}
  {\fontsize{35}{40}\texttt{\textcolor{Primary1}{\textbackslash end\{}\textcolor{Primary2}{document}\textcolor{Primary1}{\}}}}
\end{center}

\end{frame}

\end{document}


%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: xetex
%%% TeX-master: t
%%% End: 
